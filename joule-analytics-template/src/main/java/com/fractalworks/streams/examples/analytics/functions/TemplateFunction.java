package com.fractalworks.streams.examples.analytics.functions;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.annotations.AnalyticDefinition;
import com.fractalworks.streams.core.data.dictionary.NumberColumn;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.sdk.analytics.AnalyticsFunction;

import java.util.Collections;
import java.util.List;

/**
 * Template analytic function
 *
 * @author Lyndon Adams
 */
@AnalyticDefinition(
        id = "TEMPLATE_FUNCTION",
        stateless = true,
        useRawColumn = false,
        description = "Full description of function"
)
@JsonRootName(value = "template function")
public class TemplateFunction extends AnalyticsFunction<List<Double>> {

    public TemplateFunction(){
        super();
        // TODO: Add any additional constructor logic
    }

    @Override
    public List<Double> compute(NumberColumn column, Number previousValue, Context context) {
        // TODO: Add calculation logic
        return Collections.emptyList();
    }

    @Override
    public String getVariablePostFixID() {
        // TODO: Change POSTFIX_VARIABLE
        return "POSTFIX_VARIABLE";
    }
}
