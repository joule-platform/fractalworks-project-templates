/*
 * Copyright 2020-2023 FractalWorks, Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.examples.transport;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;

import java.util.Objects;

/**
 * Template consumer transport specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "templateConsumer")
public class TemplateConsumerSpecification extends AbstractTransportSpecification {

    private String someField;

    /**
     * Default and required
     */
    public TemplateConsumerSpecification() {
        super();
    }

    /**
     * Default and required
     */
    public TemplateConsumerSpecification(String name) {
        super(name);
    }

    public String getSomeField() {
        return someField;
    }

    @JsonProperty(value = "some field", required = true)
    public void setSomeField(String someField) {
        this.someField = someField;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        // TODO: Add validation logic based upon required fields
        if (someField == null || someField.isEmpty()) {
            throw new InvalidSpecificationException("someField must be provided.");
        }
    }

    @Override
    public boolean equals(Object o) {
        // TODO: Change
        if (this == o) return true;
        if (!(o instanceof TemplateConsumerSpecification)) return false;
        if (!super.equals(o)) return false;
        TemplateConsumerSpecification that = (TemplateConsumerSpecification) o;
        return someField.equals(that.someField);
    }

    @Override
    public int hashCode() {
        // TODO: Change
        return Objects.hash(super.hashCode(), someField);
    }

    @Override
    public String toString() {
        // TODO: Change
        return "TemplateConsumerTransport{" +
                "someField=" + someField +
                "} ";
    }

    @JsonIgnore
    @Override
    public Class<? extends Transport> getComponentClass() {
        // TODO: Change to your transport class
        return TemplateConsumerTransport.class;
    }
}
