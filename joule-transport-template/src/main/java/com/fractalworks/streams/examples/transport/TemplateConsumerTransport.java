/*
 * Copyright 2020-2023 FractalWorks, Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.examples.transport;

import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.transport.AbstractConsumerTransport;
import org.slf4j.LoggerFactory;

/**
 * Template publisher transport
 *
 * @author Lyndon Adams
 */
public class TemplateConsumerTransport extends AbstractConsumerTransport {

    private String someField;

    public TemplateConsumerTransport() {
        super();
    }

    public TemplateConsumerTransport(TemplateConsumerSpecification specification) {
        super(specification);
        someField = specification.getSomeField();
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        // TODO: Add custom transport initialisation code
    }


    @Override
    public void start() throws TransportException {
        super.start();
        // TODO: Add custom transport consumer code
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TemplateConsumerTransport that)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        return (someField.equals(that.someField));
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + someField.hashCode();
        return result;
    }
}
