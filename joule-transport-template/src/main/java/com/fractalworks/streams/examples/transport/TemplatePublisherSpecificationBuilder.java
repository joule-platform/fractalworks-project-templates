/*
 * Copyright 2020-2023 FractalWorks, Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.examples.transport;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;

/**
 * Template publisher specification builder
 *
 * @author Lyndon Adams
 */
public class TemplatePublisherSpecificationBuilder extends AbstractSpecificationBuilder<TemplatePublisherSpecification> {

    private String name;

    private String someField;

    public TemplatePublisherSpecificationBuilder() {
        specificationClass = TemplatePublisherSpecification.class;
    }

    public TemplatePublisherSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public TemplatePublisherSpecificationBuilder setSomeField(String someField) {
        this.someField = someField;
        return this;
    }

    @Override
    public TemplatePublisherSpecification build() throws InvalidSpecificationException {
        TemplatePublisherSpecification spec = new TemplatePublisherSpecification(name);
        spec.setSomeField(someField);
        spec.validate();
        return spec;
    }
}
