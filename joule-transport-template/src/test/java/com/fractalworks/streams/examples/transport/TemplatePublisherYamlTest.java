/*
 * Copyright 2020-2023 FractalWorks, Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.examples.transport;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Template transport unit test for valid and invalid yaml files
 *
 * @author Lyndon Adams
 */
public class TemplatePublisherYamlTest {

    @Test
    public void validTemplatePublisherYamlTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("validPublishingTemplateEvents.yaml").getFile());

        TemplatePublisherSpecificationBuilder builder = new TemplatePublisherSpecificationBuilder();
        var spec = builder.build(file);
        assertNotNull(spec);
    }

    @Test
    public void invalidTemplatePublishingYamlTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("invalidTemplatePublishingEvents.yaml").getFile());

        TemplatePublisherSpecificationBuilder builder = new TemplatePublisherSpecificationBuilder();
        assertThrows(InvalidSpecificationException.class, () ->{
            builder.build(file);
        } );
    }
}
