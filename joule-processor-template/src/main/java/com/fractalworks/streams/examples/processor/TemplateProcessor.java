/*
 * Copyright 2020-2023 FractalWorks, Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.examples.processor;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;

import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;

import java.util.Properties;

/**
 * Template processor
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "template processor")
public class TemplateProcessor extends AbstractProcessor {

    private String field;

    /**
     * Constructor is required
     */
    public TemplateProcessor(){
        super();
        super.cloneEvent = false;
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        // TODO: Add any processor initialisation logic
    }

    @Override
    public StreamEvent apply(StreamEvent streamEvent, Context context) throws StreamsException {
        metrics.incrementMetric(Metric.RECEIVED);
        if(enabled){

            // TODO: Add processing logic
            metrics.incrementMetric(Metric.PROCESSED);

        } else {
            metrics.incrementMetric(Metric.IGNORED);
        }
        return streamEvent;
    }

    public String getField() {
        return field;
    }

    @JsonProperty(value = "field",required = true)
    public void setField(final String field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "TemplateProcessor{" +
                "field='" + field + '\'' +
                '}';
    }
}
