#!/bin/bash
#
# Copyright 2020-present FractalWorks Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

JOULE_HOME=${JOULE_HOME:-$PWD}
JOULE_CLUSTER_NAME=${JOULE_CLUSTER_NAME:-jouleCluster}

joules=2

while getopts j: flag
do
    case "${flag}" in
        j) joules=${OPTARG};;
    esac
done

# Create environments
i=1;
while [ $i -le $joules ]
do
  PROCESS_NAME="joule-p$i"

  BASE_DIR="$JOULE_CLUSTER_NAME/$PROCESS_NAME"
  mkdir -p $BASE_DIR

  mkdir $BASE_DIR/META-INF
  ln -s $JOULE_HOME/META-INF/* $BASE_DIR/META-INF/

  mkdir -p $BASE_DIR/data/csv
  ln -s $JOULE_HOME/data/csv/* $BASE_DIR/data/csv/

  mkdir -p $BASE_DIR/data/parquet
  ln -s $JOULE_HOME/data/parquet/* $BASE_DIR/data/parquet/

  mkdir $BASE_DIR/conf
  ln -s $JOULE_HOME/conf/avro $BASE_DIR/conf/
  ln -s $JOULE_HOME/conf/publishers $BASE_DIR/conf/
  ln -s $JOULE_HOME/conf/sources $BASE_DIR/conf/
  ln -s $JOULE_HOME/conf/usecases $BASE_DIR/conf/
  cp $JOULE_HOME/conf/joule.properties $BASE_DIR/conf/

  mkdir $BASE_DIR/bin
  ln -s $JOULE_HOME/bin/* $BASE_DIR/bin/

  mkdir $BASE_DIR/lib
  ln -s $JOULE_HOME/lib/* $BASE_DIR/lib/

  mkdir $BASE_DIR/userlibs
  ln -s $JOULE_HOME/userlibs/* $BASE_DIR/userlibs/

  i=$((i + 1));
done
