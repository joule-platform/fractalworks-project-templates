#!/bin/bash
#
# Copyright 2020-present FractalWorks Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
JOULE_HOME=${JOULE_HOME:-$PWD}
JOULE_CLUSTER_NAME=${JOULE_CLUSTER_NAME:-jouleCluster}

JOULE_JMX_PORT=1098
JOULE_JMX_DEBUG_PORT=5005

for EXEC_DIR in "$JOULE_HOME/$JOULE_CLUSTER_NAME"/* ;
do
    cd $EXEC_DIR && ./bin/startJoule.sh -x $JOULE_JMX_PORT -d $JOULE_JMX_DEBUG_PORT
    cd "$JOULE_HOME/$JOULE_CLUSTER_NAME"

    JOULE_JMX_PORT=$((JOULE_JMX_PORT+1))
    JOULE_JMX_DEBUG_PORT=$((JOULE_JMX_DEBUG_PORT+1))
done

